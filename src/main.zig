const Board = @import("board.zig").Board;
const Config = @import("config.zig").Config;
const Player = @import("player.zig").Player;

const std = @import("std");
const print = std.debug.print;
const allocator = std.heap.page_allocator;

pub fn main() anyerror!void {
    @import("random.zig").init_rng();
    const OpenDirOptions = std.fs.Dir.OpenDirOptions;
    const cwd_path = try std.process.getCwdAlloc(allocator);
    defer allocator.free(cwd_path);
    var cwd = try std.fs.openDirAbsolute(cwd_path, OpenDirOptions{});
    defer cwd.close();
    const config_file = try cwd.openFile("7colors.conf", .{ .read = true });
    defer config_file.close();
    const config = try Config.open_from_file(config_file, allocator);

    var board = try Board.from_config(config, allocator);
    defer board.free(allocator);

    var game_allocator_buffer = try allocator.alloc(u8, (@as(usize, board.size) * @as(usize, board.size) * 10));
    defer allocator.free(game_allocator_buffer);
    var game_allocator = std.heap.FixedBufferAllocator.init(game_allocator_buffer);

    if (config.tournament_round) |rounds| {
        var game_allocator_arena = std.heap.ArenaAllocator.init(&game_allocator.allocator);
        defer game_allocator_arena.deinit();
        var p1_score: u32 = 0;
        var p2_score: u32 = 0;
        var draw: u32 = 0;
        var round: u32 = 0;
        while (round != rounds) : (round += 1) {
            // erase line
            print("\x1b[1F\x1b[K", .{});
            print("round {d}/{d}\n", .{ round, rounds });
            board.reset();
            switch (try board.game_loop(game_allocator_arena.child_allocator, false)) {
                .Player1Win => {
                    p1_score += 1;
                },
                .Player2Win => {
                    p2_score += 1;
                },
                .Draw => draw += 1,
                else => unreachable,
            }
            std.mem.swap(Player, &board.player_1, &board.player_2);
            std.mem.swap(u32, &p1_score, &p2_score);
        }
        if (rounds % 2 != 0) {
            std.mem.swap(Player, &board.player_1, &board.player_2);
            std.mem.swap(u32, &p1_score, &p2_score);
        }
        print("\x1b[1F\x1b[K", .{});
        print("round {d}/{d}\n\n", .{ rounds, rounds });
        print("{s}P1 ({}) won {d} games\x1b[0m\n", .{ board.player_1.color.foreground(), board.player_1.kind, p1_score });
        print("{s}P2 ({}) won {d} games\x1b[0m\n", .{ board.player_2.color.foreground(), board.player_2.kind, p2_score });
        if (draw > 0) {
            print("{d} draws\n", .{draw});
        }
    } else {
        _ = try board.game_loop(&game_allocator.allocator, true);
    }
}
