const std = @import("std");
const Random = std.rand.DefaultPrng;

pub var random: Random = undefined;

pub fn init_rng() void {
    random = Random.init(@bitCast(u64, std.time.timestamp()));
}
