const Color = @import("color.zig").Color;

pub const PlayerKind = enum {
    Human,
    Random,
    Random2,
    Glouton,
    GloutonPrevoyant,
};

pub const Player = struct {
    symbol: u8,
    kind: PlayerKind,
    color: Color,
    score: u32,

    pub fn is_ia(self: *const Player) bool {
        return switch (self.kind) {
            .Human => false,
            else => true,
        };
    }

    pub fn score_percent(self: *const Player, total: u32) f64 {
        return @intToFloat(f64, self.score) * 100.0 / @intToFloat(f64, total);
    }
};

pub const PlayerId = enum { P1, P2 };
