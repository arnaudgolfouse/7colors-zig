const std = @import("std");

/// Enumeration representing a terminal color.
pub const Color = enum {
    default,
    black,
    red,
    green,
    yellow,
    blue,
    magenta,
    cyan,
    white,

    /// Get the string representing this color's background.
    pub fn backgroud(self: Color) []const u8 {
        return switch (self) {
            .default => "",
            .black => "\x1b[40m",
            .red => "\x1b[41m",
            .green => "\x1b[42m",
            .yellow => "\x1b[43m",
            .blue => "\x1b[44m",
            .magenta => "\x1b[45m",
            .cyan => "\x1b[46m",
            .white => "\x1b[47m",
        };
    }

    /// Get the string representing this color's foreground.
    pub fn foreground(self: Color) []const u8 {
        return switch (self) {
            .default => "",
            .black => "\x1b[30m",
            .red => "\x1b[31m",
            .green => "\x1b[32m",
            .yellow => "\x1b[33m",
            .blue => "\x1b[34m",
            .magenta => "\x1b[35m",
            .cyan => "\x1b[36m",
            .white => "\x1b[37m",
        };
    }
};

pub const ColoredString = struct {
    data: []const u8,
    foreground: ?Color = null,
    background: ?Color = null,

    pub fn new(
        data: []const u8,
        foreground: ?Color,
        background: ?Color,
    ) ColoredString {
        return ColoredString{
            .data = data,
            .foreground = foreground,
            .background = background,
        };
    }

    pub fn format(
        self: ColoredString,
        comptime fmt: []const u8,
        options: std.fmt.FormatOptions,
        writer: anytype,
    ) std.os.WriteError!void {
        _ = options;
        if (self.background) |background| {
            try writer.print("{s}", .{background.backgroud()});
        }
        if (self.foreground) |foreground| {
            try writer.print("{s}", .{foreground.foreground()});
        }
        try writer.print("{s}\x1b[0m", .{self.data});
    }
};
