//! Configuration file

const std = @import("std");
const log = std.log;
const unicode = std.unicode;
const File = std.fs.File;

const PlayerKind = @import("player.zig").PlayerKind;
const Color = @import("color.zig").Color;

pub const Config = struct {
    board_size: u16,
    tournament_round: ?u32,
    nb_colors: u8,
    p1_kind: PlayerKind,
    p2_kind: PlayerKind,
    p1_color: Color,
    p2_color: Color,

    /// The caller will release the file.
    pub fn open_from_file(file: File, allocator: *std.mem.Allocator) !Config {
        var content: []const u8 = undefined;
        var buffer_size: usize = 32;
        while (true) : (buffer_size *= 2) {
            try file.seekTo(0);
            content = file.readToEndAlloc(allocator, buffer_size) catch continue;
            break;
        }
        defer allocator.free(content);
        return create_from_str(content);
    }

    pub fn create_from_str(data: []const u8) !Config {
        var tokenizer = Tokenizer.new(try unicode.Utf8View.init(data));
        var configuration = Config{
            .board_size = 30,
            .tournament_round = null,
            .nb_colors = 7,
            .p1_kind = PlayerKind.Human,
            .p2_kind = PlayerKind.Random2,
            .p1_color = Color.red,
            .p2_color = Color.blue,
        };
        while (try tokenizer.next()) |token| {
            switch (token) {
                .Identifier => |id| {
                    if (str_cmp(id, "P1_IA")) {
                        if (try parse_player_kind(&tokenizer)) |player_kind| {
                            configuration.p1_kind = player_kind;
                        }
                    } else if (str_cmp(id, "P2_IA")) {
                        if (try parse_player_kind(&tokenizer)) |player_kind| {
                            configuration.p2_kind = player_kind;
                        }
                    } else if (str_cmp(id, "P1_color")) {
                        if (try parse_color(&tokenizer)) |color| {
                            configuration.p1_color = color;
                        }
                    } else if (str_cmp(id, "P2_color")) {
                        if (try parse_color(&tokenizer)) |color| {
                            configuration.p2_color = color;
                        }
                    } else if (str_cmp(id, "board_size")) {
                        if ((try parse_token(&tokenizer, .Equal)) == null) {
                            continue;
                        }
                        if (try parse_number(&tokenizer)) |n| {
                            if (n > (1 << 16 - 1)) {
                                log.err("board size {d} is too large !", .{n});
                            } else {
                                configuration.board_size = @intCast(u16, n);
                            }
                        }
                    } else if (str_cmp(id, "tournament_round")) {
                        if ((try parse_token(&tokenizer, .Equal)) == null) {
                            continue;
                        }
                        if (try parse_number(&tokenizer)) |n| {
                            if (n == 0) {
                                configuration.tournament_round = null;
                            } else {
                                configuration.tournament_round = n;
                            }
                        }
                    } else if (str_cmp(id, "nb_colors")) {
                        if ((try parse_token(&tokenizer, .Equal)) == null) {
                            continue;
                        }
                        if (try parse_number(&tokenizer)) |n| {
                            var n_u8 = if (n > 255)
                                255
                            else
                                @intCast(u8, n);
                            configuration.nb_colors = n_u8;
                        }
                    } else {
                        log.err("unknown field: {s}", .{id});
                    }
                },
                else => {
                    log.err("unexpected token: {}", .{token});
                },
            }
        }
        return configuration;
    }

    fn parse_player_kind(tokenizer: *Tokenizer) !?PlayerKind {
        _ = (try parse_token(tokenizer, .Equal)).?;
        return switch ((try parse_number(tokenizer)).?) {
            0 => PlayerKind.Human,
            1 => PlayerKind.Random,
            2 => PlayerKind.Random2,
            3 => PlayerKind.Glouton,
            4 => PlayerKind.GloutonPrevoyant,
            else => |n| {
                log.err("number {d} is not a valid IA level", .{n});
                return null;
            },
        };
    }

    fn parse_color(tokenizer: *Tokenizer) !?Color {
        _ = (try parse_token(tokenizer, .Equal)).?;
        const color_str = switch ((try tokenizer.next()).?) {
            .Identifier => |id| id,
            else => return null,
        };
        if (color_from_str(color_str)) |color| {
            return color;
        } else {
            log.err("Invalid color name: {s}", .{color_str});
            return null;
        }
    }

    fn parse_token(tokenizer: *Tokenizer, token: Tokenizer.TokenTag) !?void {
        if (try tokenizer.next()) |t| {
            if (@as(Tokenizer.TokenTag, t) != token) {
                log.err("expected {}", .{token});
                return null;
            }
        } else {
            log.err("expected {}", .{token});
            return null;
        }
    }

    fn parse_number(tokenizer: *Tokenizer) !?u32 {
        switch ((try tokenizer.next()).?) {
            .Number => |n| return n,
            else => return null,
        }
    }
};

const Tokenizer = struct {
    start_ptr: usize,
    iterator: unicode.Utf8Iterator,

    const TokenTag = enum {
        Identifier,
        Number,
        /// `{`
        LBrace,
        /// `}`
        RBrace,
        /// `=`
        Equal,
        /// `,`
        Comma,
    };

    const Token = union(TokenTag) {
        Identifier: []const u8,
        Number: u32,
        /// `{`
        LBrace,
        /// `}`
        RBrace,
        /// `=`
        Equal,
        /// `,`
        Comma,

        pub fn format(
            self: Token,
            comptime fmt: []const u8,
            options: std.fmt.FormatOptions,
            writer: anytype,
        ) std.os.WriteError!void {
            _ = options;
            switch (self) {
                .Identifier => |id| try writer.print(".Identifier = \"{s}\"", .{id}),
                .Number => |n| try writer.print(".Number = {d}", .{n}),
                .LBrace => try writer.print(".LBrace", .{}),
                .RBrace => try writer.print(".RBrace", .{}),
                .Equal => try writer.print(".Equal", .{}),
                .Comma => try writer.print(".Comma", .{}),
            }
        }
    };

    pub fn new(data_utf8: unicode.Utf8View) Tokenizer {
        const x = [_]u8{'8'};

        return Tokenizer{ .start_ptr = @ptrToInt(data_utf8.bytes.ptr), .iterator = data_utf8.iterator() };
    }

    pub fn next(self: *Tokenizer) !?Token {
        var start_pos = self.iterator.i;
        while (self.iterator.nextCodepoint()) |c| : (start_pos = self.iterator.i) {
            switch (c) {
                '=' => return Token.Equal,
                '{' => return Token.LBrace,
                '}' => return Token.RBrace,
                ',' => return Token.Comma,
                '0'...'9' => {
                    var number_buffer = [20]u8{ @intCast(u8, c - '0'), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
                    var pos: usize = 1;
                    while (true) {
                        const peek_codepoint = self.iterator.peek(1);
                        if (peek_codepoint.len == 0) {
                            break;
                        }
                        const peek_c = peek_codepoint[0];
                        switch (peek_c) {
                            '0'...'9' => {
                                if (pos == number_buffer.len) {
                                    return error.NumberOverflow;
                                }
                                number_buffer[pos] = @intCast(u8, peek_c - '0');
                                pos += 1;
                                _ = self.iterator.nextCodepoint();
                            },
                            else => break,
                        }
                    }
                    if (pos == number_buffer.len and number_buffer[pos - 1] >= '4') {
                        return error.NumberOverflow;
                    }
                    var result: u32 = 0;
                    var power: u32 = 1;
                    while (pos != 0) {
                        pos -= 1;
                        result = result + power * number_buffer[pos];
                        power *= 10;
                    }
                    return Token{ .Number = result };
                },
                'a'...'z', 'A'...'Z', '_' => {
                    while (true) {
                        const peek_codepoint = self.iterator.peek(1);
                        if (peek_codepoint.len == 0) {
                            break;
                        }
                        const peek_c = unicode.utf8Decode(peek_codepoint) catch unreachable;
                        switch (peek_c) {
                            'a'...'z', 'A'...'Z', '0'...'9', '_' => {
                                _ = self.iterator.nextCodepoint();
                            },
                            else => break,
                        }
                    }
                    const end_pos = self.iterator.i;
                    return Token{ .Identifier = self.iterator.bytes[start_pos..end_pos] };
                },
                ' ', '\n', '\t' => continue,
                else => return error.UnknownChar,
            }
        }
        return null;
    }
};

fn str_cmp(s1: []const u8, s2: []const u8) bool {
    @setRuntimeSafety(false);
    if (s1.len != s2.len) {
        return false;
    }
    for (s1) |c, index| {
        if (c != s2[index]) {
            return false;
        }
    }
    return true;
}

fn cmp_char_case_insensitive(c1: u21, c2: u21) bool {
    if (c1 == c2) {
        return true;
    }
    switch (c1) {
        'A'...'Z' => {
            switch (c2) {
                'a'...'z' => return (c1 + 'a' - 'A') == c2,
                else => return false,
            }
        },
        'a'...'z' => {
            switch (c2) {
                'A'...'Z' => return (c1 + 'A' - 'a') == c2,
                else => return false,
            }
        },
        else => return false,
    }
}

/// Compare two UTF-8 strings case insensitively (only for equality).
///
/// Case-insensitivity is only checked on 'a'...'z' and 'A'...'Z'.
///
/// # Safety
/// The two strings **must** be valid UTF-8.
fn str_cmp_case_insensitive(value: []const u8, reference: []const u8) bool {
    @setRuntimeSafety(false);
    if (value.len != reference.len) {
        return false;
    }

    var value_unicode = unicode.Utf8View.initUnchecked(value).iterator();
    var reference_unicode = unicode.Utf8View.initUnchecked(reference).iterator();

    while (value_unicode.nextCodepoint()) |value_codepoint| {
        if (reference_unicode.nextCodepoint()) |reference_codepoint| {
            if (!cmp_char_case_insensitive(value_codepoint, reference_codepoint)) {
                return false;
            }
        } else {
            return false;
        }
    }
    return true;
}

pub fn color_from_str(color: []const u8) ?Color {
    if (str_cmp_case_insensitive(color, "default")) {
        return .default;
    } else if (str_cmp_case_insensitive(color, "black")) {
        return .black;
    } else if (str_cmp_case_insensitive(color, "red")) {
        return .red;
    } else if (str_cmp_case_insensitive(color, "green")) {
        return .green;
    } else if (str_cmp_case_insensitive(color, "yellow")) {
        return .yellow;
    } else if (str_cmp_case_insensitive(color, "blue")) {
        return .blue;
    } else if (str_cmp_case_insensitive(color, "magenta")) {
        return .magenta;
    } else if (str_cmp_case_insensitive(color, "cyan")) {
        return .cyan;
    } else if (str_cmp_case_insensitive(color, "white")) {
        return .white;
    }
    return null;
}
