const std = @import("std");
const panic = std.debug.panic;
const Allocator = std.mem.Allocator;
const ArrayListUnmanaged = std.ArrayListUnmanaged;
const AllocationError = Allocator.Error;

const Board = @import("board.zig").Board;
const PlayerId = @import("player.zig").PlayerId;
const Player = @import("player.zig").Player;

/// # Note
/// `allocator` must be able to allocate four more boards.
pub fn get_player_move(board: *const Board, allocator: *Allocator, comptime player_id: PlayerId) AllocationError!u8 {
    const player = switch (player_id) {
        .P1 => &board.player_1,
        .P2 => &board.player_2,
    };
    switch (player.kind) {
        .Human => return get_human_move(board),
        .Random => return get_random_move(board),
        .Random2 => return get_random_move_2(board, allocator, player),
        .Glouton => return get_glouton_move(board, allocator, player_id),
        .GloutonPrevoyant => return get_glouton_prevoyant_move(board, allocator, player_id),
    }
}

fn get_human_move(board: *const Board) u8 {
    const stdout = std.io.getStdOut().writer();
    const stdin = std.io.getStdIn().reader();

    stdout.writeAll("Choose a letter :\n") catch panic("stdout error\n", .{});

    while (true) {
        const letter = stdin.readByte() catch panic("stdin error\n", .{});
        if (letter == '\n') {
            continue;
        }

        const and_then = stdin.readByte() catch panic("stdin error\n", .{});
        if (and_then != '\n') {
            std.debug.print("Only one letter may be given as input !\n", .{});
            while (true) {
                if ((stdin.readByte() catch panic("stdin error\n", .{})) == '\n') {
                    break;
                }
            }
            continue;
        }

        if (letter >= 'A' and letter <= 'A' + board.nb_colors) {
            return letter;
        } else if (letter >= 'a' and letter <= 'a' + board.nb_colors) {
            return letter - 'a' + 'A';
        } else {
            std.debug.print("incorrect letter : {c}\n", .{letter});
        }
    }
    return unreachable;
}

fn get_random_move(board: *const Board) u8 {
    return (@import("random.zig").random.random.int(u8) % board.nb_colors) + 'A';
}

fn get_random_move_2(board: *const Board, allocator: *Allocator, player: *const Player) AllocationError!u8 {
    var adjacent = try ArrayListUnmanaged(u8).initCapacity(allocator, player.score);
    defer adjacent.deinit(allocator);

    var line: u16 = 0;
    var col: u16 = undefined;
    while (line < board.size) : (line += 1) {
        col = 0;
        while (col < board.size) : (col += 1) {
            const tile = board.tiles[col + board.size * line];
            if (tile == player.symbol or tile == player.symbol) {
                continue;
            }
            const neighbors = board.neighbors(col, line);
            var contains: bool = false;
            for (neighbors) |neighbor_opt| {
                if (neighbor_opt) |neighbor| {
                    if (neighbor.cell == player.symbol) {
                        contains = true;
                        break;
                    }
                }
            }
            if (contains) {
                try adjacent.append(allocator, tile);
            }
        }
    }

    if (adjacent.items.len == 0) {
        return 'A';
    }
    const result_index = @import("random.zig").random.random.int(usize);
    return adjacent.items[result_index % adjacent.items.len];
}

/// # Note
/// `allocator` must be able to allocate two more boards.
fn get_glouton_move(board: *const Board, allocator: *Allocator, comptime player_id: PlayerId) AllocationError!u8 {
    var best_color: u8 = 'A';
    var best_score: u32 = 0;
    var color: u8 = 'A';
    while (color < 'A' + board.nb_colors) : (color += 1) {
        var board_clone: Board = try board.clone(allocator);
        defer board_clone.free(allocator);
        try board_clone.make_move(color, allocator, player_id);
        const score = board_clone.get_player_score(player_id);
        if (score > best_score) {
            best_score = score;
            best_color = color;
        }
    }
    return best_color;
}

/// # Note
/// `allocator` must be able to allocate four more boards.
fn get_glouton_prevoyant_move(board: *const Board, allocator: *Allocator, comptime player_id: PlayerId) AllocationError!u8 {
    var best_color: u8 = 'A';
    var best_score: u32 = 0;
    var color: u8 = 'A';
    var color2: u8 = undefined;
    const current_score = board.get_player_score(player_id);

    while (color < 'A' + board.nb_colors) : (color += 1) {
        var board_clone: Board = try board.clone(allocator);
        defer board_clone.free(allocator);
        try board_clone.make_move(color, allocator, player_id);
        if (current_score == board_clone.get_player_score(player_id)) {
            // useless move, but the next one might not be !
            continue;
        }
        color2 = 'A';
        while (color2 < 'A' + board.nb_colors) : (color2 += 1) {
            if (color == color2) {
                continue;
            }
            var board_clone_2: Board = try board_clone.clone(allocator);
            defer board_clone_2.free(allocator);
            try board_clone_2.make_move(color2, allocator, player_id);
            const score2 = board_clone_2.get_player_score(player_id);
            if (score2 > best_score) {
                best_color = color;
                best_score = score2;
            }
        }
    }
    return best_color;
}
