const std = @import("std");
const Allocator = std.mem.Allocator;

const Color = @import("color.zig").Color;
const Player = @import("player.zig").Player;
const PlayerKind = @import("player.zig").PlayerKind;
const PlayerId = @import("player.zig").PlayerId;
const Config = @import("config.zig").Config;
var random = &@import("random.zig").random;

pub const Board = struct {
    tiles: []u8,
    size: u16,
    nb_colors: u8,
    player_1: Player,
    player_2: Player,

    pub const CreateError = error{TooManyColors} || AllocationError;
    const GameState = enum { Continue, Player1Win, Player2Win, Draw };
    const AllocationError = std.mem.Allocator.Error;

    pub fn from_config(config: Config, allocator: *Allocator) CreateError!Board {
        return Board.create(allocator, config.board_size, config.nb_colors, Player{
            .symbol = 0,
            .kind = config.p1_kind,
            .color = config.p1_color,
            .score = 1, // each player has 1 tile at the beginning
        }, Player{
            .symbol = 1,
            .kind = config.p2_kind,
            .color = config.p2_color,
            .score = 1,
        });
    }

    pub fn create(allocator: *Allocator, size: u16, nb_colors: u8, player_1: Player, player_2: Player) CreateError!Board {
        if (nb_colors > 26) {
            return error.TooManyColors;
        }
        var tiles = try allocator.alloc(u8, size * size);
        fill_random_letters(nb_colors, tiles, player_1.symbol, player_2.symbol);
        return Board{ .tiles = tiles, .size = size, .nb_colors = nb_colors, .player_1 = player_1, .player_2 = player_2 };
    }

    /// Reset the board
    /// 
    /// This randomize the tiles again, and reset the players' scores.
    pub fn reset(self: *Board) void {
        fill_random_letters(self.nb_colors, self.tiles, self.player_1.symbol, self.player_2.symbol);
        self.player_1.score = 1;
        self.player_2.score = 1;
    }

    fn fill_random_letters(nb_colors: u8, tiles: []u8, p1_symbol: u8, p2_symbol: u8) void {
        for (tiles) |_, index| {
            tiles[index] = (random.random.int(u8) % nb_colors) + 'A';
        }
        tiles[0] = p1_symbol;
        tiles[tiles.len - 1] = p2_symbol;
    }

    /// Create a clone of this board.
    ///
    /// It will need to be freed using `free`.
    pub fn clone(self: *const Board, allocator: *Allocator) !Board {
        const tiles = try allocator.alloc(u8, self.tiles.len);
        for (self.tiles) |value, index| {
            tiles[index] = value;
        }
        return Board{ .tiles = tiles, .size = self.size, .nb_colors = self.nb_colors, .player_1 = self.player_1, .player_2 = self.player_2 };
    }

    pub fn free(self: Board, allocator: *Allocator) void {
        allocator.free(self.tiles);
    }

    pub fn get_cell(self: *const Board, x: u16, y: u16) error{OutOfBounds}!u8 {
        if (x >= self.size or y >= self.size) {
            return error.OutOfBounds;
        }
        const pos: u32 = x + self.size * y;
        return self.tiles[pos];
    }

    pub fn set_cell(self: *Board, x: u16, y: u16, value: u8) error{OutOfBounds}!void {
        if (x >= self.size or y >= self.size) {
            return error.OutOfBounds;
        }
        const pos: u32 = x + self.size * y;
        self.tiles[pos] = value;
    }

    pub fn get_cell_color(value: u8) ?Color {
        return switch (value) {
            'A' => .red,
            'B' => .blue,
            'C' => .green,
            'D' => .magenta,
            'E' => .cyan,
            'F' => .yellow,
            'G' => .white,
            else => null,
        };
    }

    pub fn get_player_score(board: *const Board, comptime player_id: PlayerId) u32 {
        return switch (player_id) {
            .P1 => board.player_1.score,
            .P2 => board.player_2.score,
        };
    }

    pub fn end_game(self: *const Board) GameState {
        const total_tiles = (@as(u32, self.size) * @as(u32, self.size));
        if (self.player_1.score * 2 > total_tiles) {
            return GameState.Player1Win;
        } else if (self.player_2.score * 2 > total_tiles) {
            return GameState.Player2Win;
        } else if (self.player_1.score + self.player_2.score == total_tiles) {
            return GameState.Draw;
        } else {
            return GameState.Continue;
        }
    }

    fn print_welcome() void {
        const print = std.debug.print;
        // move cursor to (0, 0), clear screen
        print("\x1b[H\x1b[0J", .{});
        print("\n", .{});
        print("\n", .{});
        print("Welcome to the 7 wonders of the world of the 7 colors\n", .{});
        print("*****************************************************\n", .{});
        print("\n", .{});
    }

    pub fn game_loop(self: *Board, allocator: *Allocator, display: bool) !GameState {
        const print = std.debug.print;
        const get_player_move = @import("game.zig").get_player_move;

        if (display) {
            print_welcome();
            print("{}\n", .{self});
        }
        var game_state: GameState = .Continue;
        while (game_state == .Continue) {
            if (display) {
                print("{s}P1 turn to play\x1b[0m\n", .{self.player_1.color.foreground()});
            }
            const move_p1 = try get_player_move(self, allocator, .P1);
            try self.make_move(move_p1, allocator, .P1);
            if (display) {
                print("{}\n", .{self});
            }
            game_state = self.end_game();
            if (game_state != .Continue) {
                break;
            }
            if (display) {
                print("{s}P2 turn to play\x1b[0m\n", .{self.player_2.color.foreground()});
            }
            const move_p2 = try get_player_move(self, allocator, .P2);
            try self.make_move(move_p2, allocator, .P2);
            if (display) {
                print("{}\n", .{self});
            }
            game_state = self.end_game();
        }
        if (display) {
            switch (game_state) {
                .Player1Win => print("{s}P1 WIN !\x1b[0m\n", .{self.player_1.color.foreground()}),
                .Player2Win => print("{s}P2 WIN !\x1b[0m\n", .{self.player_2.color.foreground()}),
                .Draw => print("It's a draw !", .{}),
                else => {},
            }
        }
        return game_state;
    }

    /// # Note
    /// `allocator` must be able to allocate one more board.
    pub fn make_move(self: *Board, move: u8, allocator: *Allocator, comptime player_id: PlayerId) AllocationError!void {
        var player = switch (player_id) {
            .P1 => &self.player_1,
            .P2 => &self.player_2,
        };
        var to_process = try std.ArrayListUnmanaged(struct { column: u16, line: u16 }).initCapacity(allocator, player.score);
        defer to_process.deinit(allocator);

        var x: u16 = 0;
        var y: u16 = undefined;
        while (x < self.size) : (x += 1) {
            y = 0;
            while (y < self.size) : (y += 1) {
                const index = x * self.size + y;
                if (self.tiles[index] == player.symbol) {
                    try to_process.append(allocator, .{ .column = y, .line = x });
                }
            }
        }

        while (to_process.popOrNull()) |position| {
            const index = position.column + self.size * position.line;
            if (self.tiles[index] == move) {
                self.tiles[index] = player.symbol;
                player.score += 1;
            }
            for (self.neighbors(position.column, position.line)) |neighbor| {
                if (neighbor) |cell| {
                    if (cell.cell == move) {
                        try to_process.append(allocator, .{ .column = cell.column, .line = cell.line });
                    }
                }
            }
        }
    }

    const Neighbors = [4]?struct { cell: u8, column: u16, line: u16 };

    /// Returns N, E, S, W neighbors (if they exists).
    ///
    /// # Safety
    /// `column` and `line` must be in bounds.
    pub fn neighbors(self: *const Board, column: u16, line: u16) Neighbors {
        var cell_neighbors = Neighbors{ null, null, null, null };
        if (line > 0) {
            cell_neighbors[0] = .{ .cell = self.tiles[column + (line - 1) * self.size], .column = column, .line = line - 1 };
        }
        if (column > 0) {
            cell_neighbors[1] = .{ .cell = self.tiles[column - 1 + line * self.size], .column = column - 1, .line = line };
        }
        if (line < self.size - 1) {
            cell_neighbors[2] = .{ .cell = self.tiles[column + (line + 1) * self.size], .column = column, .line = line + 1 };
        }
        if (column < self.size - 1) {
            cell_neighbors[3] = .{ .cell = self.tiles[column + 1 + line * self.size], .column = column + 1, .line = line };
        }
        return cell_neighbors;
    }

    pub fn format(
        self: *const Board,
        comptime fmt: []const u8,
        options: std.fmt.FormatOptions,
        writer: anytype,
    ) std.os.WriteError!void {
        for (self.tiles) |tile, index| {
            if (tile == self.player_1.symbol) {
                try writer.print("{s}{s} \x1b[0m", .{ self.player_1.color.foreground(), self.player_1.color.backgroud() });
            } else if (tile == self.player_2.symbol) {
                try writer.print("{s}{s} \x1b[0m", .{ self.player_2.color.foreground(), self.player_2.color.backgroud() });
            } else {
                const color = char_color(tile);
                try writer.print("{s}{c}", .{ color.foreground(), tile });
            }
            if ((index +% 1) % self.size == 0) {
                try writer.writeAll("\n");
            }
        }
        try writer.print("\n{s}P1 score : {d:2.2}\x1b[0m\n", .{ self.player_1.color.foreground(), self.player_1.score_percent(self.size * self.size) });
        try writer.print("{s}P2 score : {d:2.2}\x1b[0m\n", .{ self.player_2.color.foreground(), self.player_2.score_percent(self.size * self.size) });
    }

    fn char_color(c: u8) Color {
        return switch (c) {
            'A' => .red,
            'B' => .blue,
            'C' => .green,
            'D' => .magenta,
            'E' => .cyan,
            'F' => .yellow,
            'G' => .white,
            else => .default,
        };
    }
};
