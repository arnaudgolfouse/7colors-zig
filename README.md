Little CLI game to learn the [Zig programming language](https://ziglang.org/).

Run with `zig build run`.

## Options

The game can be configured via the [**7colors.conf**](./7colors.conf) file. Available options are

- `PI_IA = <ia_level>` (default = 0): configure the IA level for player 1.
- `P2_IA = <ia_level>` (default = 2): configure the IA level for player 2.
- `P1_color = <color>` (default = red): configure the color for player 1.
- `P2_color = <color>` (default = blue): configure the color for player 2.
- `board_size = <int>` (default = 30): size of the board. cannot be `0`.
- `tournament_round = <int>` (default = 0): number of games to play in a row. Set to `0` to disable tournament mode.
- `nb_colors = <int>` (default = 7): unused for now.

Types of configuration options:

- ia_level:
  - `0`: Human
  - `1`: IA chose a color randomly each turn
  - `2`: IA chose randomly, weighted by how many of the letter are in its immediate neighborhood
  - `3`: IA chose the best color for the next turn
  - `4`: IA chose the best color for the next 2 turns
- color (case-insensitive):
  - default (invisible)
  - black
  - red
  - green
  - yellow
  - blue
  - magenta
  - cyan
  - white
